using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Crop", menuName = "Crop")]
public class CropAsset : ScriptableObject
{
	public Sprite seedSprite;
	public Sprite youngSprite;
	public Sprite doneSprite;
    public Sprite finalSprite;
    public int price;
	public bool seedIsOnGround = false;
}
