﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts
{
    [System.Serializable]
    public class Player : MonoBehaviour
    {
        public string Name;
        public int Cash;
        public List<Crop> Inventory;
        public GameObject CurrentSeed;

        private Crop c_Crop;
        private void Awake()
        {
            Cash = 25;
            Inventory = new List<Crop>();
        }
        public void BuySeeds(int price, Crop c)
        {
            Cash -= price;
            SetCrop(c);
            Inventory.Add(c_Crop);
            c_Crop = null;
            Debug.Log("INVENTARIO: " + Inventory.Count);
            Debug.Log("DINHEIRO: " + Cash);
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            Soil soil = collision.gameObject.GetComponent<Soil>();

            if (Input.GetKey(KeyCode.Space))
            {
                SpriteRenderer img = CurrentSeed.GetComponentInChildren<SpriteRenderer>();
                SetCrop(GetCrop(img.sprite));
                img.sprite = c_Crop.GetCropSprite();
                Color tmp = img.color;
                tmp.a = 0f;
                img.color = tmp;
                soil.overlay.sprite = c_Crop.GetCropSprite();
                RemoveCrop(Inventory, c_Crop);
            }

        }

        public void SetCrop(Crop c)
        {
            c_Crop = c;
            SpriteRenderer img = CurrentSeed.GetComponentInChildren<SpriteRenderer>();
            img.sprite = c_Crop.GetCropSprite();
            Color tmp = img.color;
            tmp.a = 1f;
            img.color = tmp;

        }

        public Crop GetCrop(Sprite s)
        {
            for (int i = 0; i < Inventory.Count; i++)
            {
                if (Inventory[i].GetCropSprite() == s)
                {
                    return Inventory[i];
                }

            }
            return null;
        }

        private void RemoveCrop(List<Crop> Inv, Crop c)
        {
            for (int i = 0; i > Inv.Count; i++)
            {
                if (Inv[i] == c)
                {
                    Inv.RemoveAt(i);
                    break;
                }
            }
        }
    }
}