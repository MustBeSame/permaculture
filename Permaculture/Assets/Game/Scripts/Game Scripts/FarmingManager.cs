﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FarmingManager : MonoBehaviour
{

    public Tilemap ground;

    public Tile tile;

    void Start()
    {

    }



    public void Update() //Mudar quando houver click OnMouseDown()
    {
        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int coordinate = ground.WorldToCell(mouseWorldPos);

        if (Input.GetMouseButtonDown(0))
        {
            //Pinta o tile selecionado com o solo para plantio
            ground.SetTile(coordinate, tile);
        }
    }
}
