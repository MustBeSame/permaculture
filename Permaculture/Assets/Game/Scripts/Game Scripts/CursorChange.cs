﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorChange : MonoBehaviour
{
    public Texture2D cursorSprite;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    public void ChangeCursor()
    {
        Cursor.SetCursor(cursorSprite, hotSpot, cursorMode);
    }
}
