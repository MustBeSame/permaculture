﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts
{
    public class Shop_Manager : MonoBehaviour
    {
        public List<CropAsset> seeds;
        public GameObject i;

        public Player p;
        private bool playerOn = false;

        public void SellCorn()
        {
            if (!playerOn)
            {
                p = FindObjectOfType<Player>();
                playerOn = false;
                Crop cp = new Crop(seeds[0]);

                p.BuySeeds(seeds[0].price, cp);

                FillInventory(cp);
            }
        }
        public void SellTomato()
        {
            if (!playerOn)
            {
                p = FindObjectOfType<Player>();
                playerOn = false;
                Crop cp = new Crop(seeds[1]);

                p.BuySeeds(seeds[1].price, cp);

                FillInventory(cp);
            }
        }

        private void FillInventory(Crop c)
        {
            Transform[] inventorySlots = i.GetComponentsInChildren<Transform>();

            if (inventorySlots != null)
            {
                bool done = false;
                foreach (Transform slot in inventorySlots)
                {
                    if (!done)
                    {
                        if (slot.tag == "Inventory_Slot")
                        {
                            Image img = slot.GetComponentInChildren<Image>();
                            Text txt = slot.GetComponentInChildren<Text>();

                            if (img.sprite == null)
                            {
                                img.sprite = c.GetCropSprite();
                                Color tmp = img.color;
                                tmp.a = 1f;
                                img.color = tmp;
                                done = true;
                            }
                            else if (img.sprite == c.GetCropSprite())
                            {
                                img.sprite = c.GetCropSprite();
                                Color tmp = img.color;
                                tmp.a = 1f;
                                img.color = tmp;
                                txt.text = (Convert.ToInt32((txt.text == string.Empty ? 1 : Convert.ToInt32(txt.text))) + 1).ToString();
                                done = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
