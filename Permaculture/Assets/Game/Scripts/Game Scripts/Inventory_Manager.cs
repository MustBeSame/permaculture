﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game.Scripts
{
    public class Inventory_Manager : MonoBehaviour
    {
        [SerializeField]
        private Player p;

        private bool playerOn = false;


        private void FixedUpdate()
        {

            if (Input.GetKey(KeyCode.I))
            {
                this.gameObject.SetActive(true);
            }
            if (!playerOn)
            {
                p = FindObjectOfType<Player>();
                playerOn = false;
            }
        }
        public void GrabSeed()
        {
            Crop c = p.GetCrop(this.gameObject.GetComponentInChildren<Image>().sprite);

            p.SetCrop(c);
        }

        public void ClearSlot(GameObject slot)
        {

            Image img = slot.GetComponentInChildren<Image>();
            Text txt = slot.GetComponentInChildren<Text>();

            if (img.sprite != null)
            {
                int count = Convert.ToInt32((txt.text == string.Empty ? 1 : Convert.ToInt32(txt.text)));
                if (count > 1)
                {
                    txt.text = (Convert.ToInt32(txt.text) - 1).ToString();
                }
                else
                {
                    //img.sprite = null;
                    Color tmp = img.color;
                    tmp.a = 0f;
                    img.color = tmp;
                }
            }
        }
    }
}